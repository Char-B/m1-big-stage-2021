# M1 BIG - Stage 2021

* Ensemble de fonctions dédiées à l'automatisation de l'analyses des scores de prédiction des effets des variants (faux-sens).

* Contexte : Identifier les scores les plus pertinents, parmi 31, pour établir une stragégie optmisées de prédiction réduite aux ~4-6 "meilleurs" scores identifiés.

* Critères de tri : corrélation + agrément (redondance) + performance (AUC)
